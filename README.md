# TaskFarmer Hands-On Training #

Materials for a NERSC training session provided to JGI users on June 28, 2017, 3-5pm in room 149B at the JGI campus in Walnut Creek, CA.

# Get started #

First, download the data set we'll be using today. 

```
#!shell

git clone git@bitbucket.org:dwudwary/tftraining.git
```

Presentation slides are at:
[NERSC's webpage](https://www.nersc.gov/users/computational-systems/genepool/genepool-training-and-tutorials/)

#Example 1#
Use the submission scripts in tftraining/example1 to submit a taskfarmed job. Completed output is found in tftraining/example1_done

Files:

`submission.sh` - an example Cori submission script. Uses a node reservation that will not be available outside the training session

`uge_submission.sh` - an example Genepool submission script. 

`tasks.txt` - the task list that taskfarmer will run through. Feel free to modify as you like!

Modify the submission scripts to match your directory structure, and change scheduler flags as needed. 

#Example 2#

A sample problem for you to solve. Use TaskFarmer to run a batch of unannotated FASTA-format genome sequences through the gene prediction tool, Glimmer. The Glimmer executable and model file are in `tftraining/example2`. Genome sequences are found in `tftraining/genomes`. If you're doing this during the training session, use the Cori reservation "jgitrain". You can use the example1 scripts as a starting point.

Files in tftraining/example2:

`glimmer3` - the GLIMMER 3.02 executable. Will run on Cori and Genepool

`build-icm` - the executable used to build the Streptococcus gene model

`model.icm` - a Streptococcus gene model built from `Spyo_sequence.txt` using `build-icm`

`Spyo_sequence.txt` - A fasta list of 200 Streptococcus pyogenes gene sequences.