#!/bin/bash -l
#SBATCH -N 2 		## Need at least two nodes
#SBATCH -c 64 		## Number of CPUs availble for the TF job - NOT each TF task
#SBATCH -t 00:05:00 	## Run time overall - NOT per task
#SBATCH -p regular
#SBATCH --reservation=jgitrain ## This will not be available outside of training session
#SBATCH -C haswell

cd $BSCRATCH/tftraining/example1 # Or whichever you directory you cloned into
module load taskfarmer #to set $PATH, primarily
export THREADS=32 # how many tftasks can run at once?
runcommands.sh tasks.txt
