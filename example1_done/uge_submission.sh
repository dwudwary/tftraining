#!/bin/bash -l
#$ -pe pe_16 32 	## Need at least two nodes
#$ -l h_rt=00:05:00 	## Run time overall - NOT per task

cd $BSCRATCH/training/tftraining/example1 # Or whichever you directory you cloned into
#export PATH=/global/common/cori/tig/taskfarmer/1.5/bin:$PATH #to set $PATH
module load taskfarmer
module load openmpi
export THREADS=32 # how many tftasks can run at once?
runcommands.sh tasks.txt
